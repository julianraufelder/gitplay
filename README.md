# Git-Tutorial LUG-VS

Inhalt

[[_TOC_]]

## Git-Lab-Account anlegen

Öffne https://gitlab.com mit deinen Webbrowser (z.B. LibreWolf oder Firefox)

Melde dich bei Git-Lab an, oder registriere dich, falls du noch keinen Git-Lab-Account hast.

## Installiere Git auf Deiner Hardware (unter Linux-Betriebssystemen) mit dem Befehl:
Unter openSUSE (Tumbleweed, Leap) und Distros die
den Zypper-Paketmanager verwenden:
```
$ sudo zypper in git
```

Unter Ubuntu und Derivaten:
```
$ sudo apt-get install git
```

## SSH-Key erzeugen und bei Git-Lab hinterlegen

Falls noch nicht geschehen, lege einen ssh key an
```
$ ssh-keygen
```
 -> wenn nicht anders gewünscht, drei mal "Enter". Falls Du aber eine Passwortabfrage wünschst (macht mehr Arbeit und ist bei diesem Projekt eigentlich unnötig), gib, wie abgefragt, zwei mal das gleiche (sichere) Passwort ein.

Anschließend den public Key ausgeben und per Copy&Paste bei Gitlab hinzufügen (Gitlab Profilbild -> "Preferences" -> "SSH Keys"
```
cat $HOME/.ssh/id_rsa.pub
```

## Wichtige Befehle
```
$ git status
```
--> sollte einem "in Fleisch und Blut" übergehen; Julian kann das hier gerne erklären  ;-)
```
$ git pull
```
--> mit diesem Befehl holt man sich Änderungen, die Projektmitarbeiter auf Git hochgeladen haben, damit man diese lokal auf seinem Rechner hat und in der aktuellsten Version weiterarbeiten kann
```
$ git push
```
--> mit diesem Befehl lädt man die eigenen Änderungen auf Git für die Projektmitarbeiter hoch (je nach dem, die wie Einstellungen sind, kann dann ein editierter Personenkreis, oder gar jeder, der den Link kennt, diese einsehen)
```
$ git clone https://gitlab.com/
```
--> mit diesem Befehl kopiert man sich den kompletten Ordner von Git auf seine lokale Festplatte

==> Voraussetzungen:
* man hat sich schon einen Schlüssel erstellt und diesen in seinen persönlichen Git-Account-Einstellungen hinterlegt (wer beschreibt, wie das geht?)
* man hat sich den SSH- oder HTTPS-Link kopiert, indem man im entsprechenden Git-Projekt auf den Button ganz oben rechts "Clone" geklickt hat (kopiert den Link, welcher nachher in der Konsole eingefügt werden muss)

```
$ git commit
```
--> Beschreibung fehlt noch

## Beispiel einer Bearbeitung des aktuellen Projekts

### 0. Vorbereitungen, um selbst ein Projekt zu erstellen (Klonen)
Als erstes sollte man sich im lokalen /home-Verzeichnis (bei Linux "/home/BenutzerName" erreichbar = "cd ~") ein entsprechenes Unterverzeichnis für die Git-Projekte erstellen
```
mkdir -P ~/git
cd ~/git
```
--> der Befehl "mkdir" erstellt den Ordner, die Option "-p", bzw. --parents (oder doch -P)??? ==> BITTE ERKLÄREN; ich kapiere nicht, was die Option bedeutet, habe über "mkdir --help" nachgelesen ...
--> mit "cd ~/git" wechselt man in den neu angelegten Ordner (im Homeverzeichnis des Benutzers)

(Anstatt "git" könnte man das Verzeichnis "SW-Projekte" nennen) ==> warum und für was steht "SW"??

```
git clone https://gitlab.com/julianraufelder/gitplay
```
Mit dem Kommando "clone" klont / kopiert man ein Projekt auf ein lokles Unterverzeichnis mit dem Namen des Projektes (hier "gitplay")

### 1. Wechsle in der Konsole in Dein Verzeichnis "git" (in diesem Beispiel liegt es unter /home/holger) und dann in das Unterverzeichnis "gitplay" (unserem derzeitigen Projekt)
```
$ cd git/gitplay
```
Ein weiterer Befehl zeigt dir die im Unterverzeichnis (dem Projekt) enthaltenen Datei(en) an:
```
$ ls -l
```
Dort finden wir die Datei "README.md", in welcher jeder Projektteilnehmer lokal arbeitet und sie nach dem Schreiben der Änderungen auf Git hochlädt. Wie das geht, folgt nun ...

### 2. Holen (pull) der zu bearbeitenden Datei (README.md):
```
$ git status
```
dann
```
$ git pull
```
### 3. Öffnen der heruntergeladenen Datei (im schon betretenden Verzeichnis Deiner Konsole) mit einem Editor Deiner Wahl (Kate, Vim, Joe, etc.)
```
$ kate README.md
```
Nun kannst Du im Editor (hier Kate) die Datei bearbeiten und dann abspeichern (unter Kate: Strg+S)

### 4. Weitere Befehle, um die Datei zu prüfen, mit einem Kommentar zu versehen und sie auf Git hochzuladen
```
$ git status
```

```
$ git commit -a
```
--> Es öffnet sich der vom Betriebssystem (Linux) voreingestellte Editor und Du gibst ganz oben einen kurzen, aussagekräftigen Kommentar bezüglich der Änderung ein und speicherst das Ganze ab.
==> Dazu muss man natürlich wissen, wie der Editor (Vim, Joe, etc.) mit den entsprechenden Shortcuts bedient wird.
* Vim: https://www.df.eu/blog/vim-editor-basistipps-zur-nutzung/
* Joe: https://de.wikibooks.org/wiki/Linux-Praxisbuch/_Konsole/_joe
```
$ git push
```
--> der Befehl "pusht / schiebt" die Datei hoch in das gemeinsam genutzte Git-Projekt (also ins Internet)

### 5. Wechsle in Deinen Browser, in dem das Git-Projekt geöffnet ist und lade die Seite mit F5 neu.
Nun solltest Du die zuvor lokal gemachten Änderungen online sehen; und nicht nur Du, sondern auch alle anderen Projektmitarbeiter.  :-)
