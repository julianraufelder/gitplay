# tables :-)

## Help Syntax
|Syntax | Meaning |
| :--- | :--- |
|[OPTION]| optional |
|\<OPTION\>| mandatory |
|...| "and so on" |

## Common Shell Commands
|Usage | Description |
| :--- | :--- |
| ls [OPTION]... [FILE]... | list files |
| cd [OPTION]... DIR... | change directory |
| mkdir [OPTION]... DIR... | make directory |
| rmdir [OPTION]... DIR... | remove directory |
| rm [OPTION]... [FILE]... | remove file |
| chmod [OPTION]... MODE[,MODE]... FILE... | change mode |
| chown [OPTION]... [OWNER][:[GROUP]] FILE... | change owner:group |

